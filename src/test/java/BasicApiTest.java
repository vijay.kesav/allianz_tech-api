import org.testng.Assert;
import org.testng.annotations.*;
import utils.ApiUtils;

public class BasicApiTest extends BaseTest{

    @Test
    public void validate_the_status_code_for_200() {
   
        res = ApiUtils.getResponsebyPath("/api");
        jp = ApiUtils.getJsonPath(res);
        testUtils.checkStatusIs200(res);
    }

    @Test
    public void validate_the_value_of_key_answer() {
        res = ApiUtils.getResponsebyPath("/api");
        jp = ApiUtils.getJsonPath(res);
        String temp=jp.get("answer");
        String Expected=null;
        if(temp.equals("yes"))
        	Expected="yes";
        if(temp.equals("no"))
        	Expected="no";
        if(temp.equals("maybe"))
        	Expected="maybe"; 
        Assert.assertEquals(temp,Expected, "Validation for value of key answer Failed");
    }

    @Test
    public void validate_the_value_of_key_forced() {
        res = ApiUtils.getResponsebyPath("/api");
        jp = ApiUtils.getJsonPath(res);
        boolean temp=jp.get("forced");
        Assert.assertEquals(temp,false,"Validation for value of key forced Failed");
    }
    
    @Test
    public void validate_the_value_of_key_forced_image() {
        res = ApiUtils.getResponsebyPath("/api");
        jp = ApiUtils.getJsonPath(res);
        String temp=jp.get("image");
        Assert.assertTrue(temp.matches("https://.*.gif"));
    }
}
